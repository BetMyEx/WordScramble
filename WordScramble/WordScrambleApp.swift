//
//  WordScrambleApp.swift
//  WordScramble
//
//  Created by Admin on 12.01.2022.
//

import SwiftUI

@main
struct WordScrambleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
