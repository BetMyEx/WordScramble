//
//  ContentView.swift
//  WordScramble
//
//  Created by Admin on 12.01.2022.
//

import SwiftUI

struct ContentView: View {
    @State private var usedWords = [String]()
    @State private var rootWord = ""
    @State private var newWord = ""
    @State private var characterScore = 0
    @State private var wordScore = 0
    
    @State private var errorTittle = ""
    @State private var errorMessage = ""
    @State private var showingAllert = false
    
    var body: some View {
        NavigationView {
            List {
                Section {
                    TextField("Enter your word", text: $newWord)
                        .autocapitalization(.none)
                }
                
                Section{
                    ForEach(usedWords, id: \.self) { word in
                        HStack{
                            Image(systemName: "\(word.count).circle.fill")
                            Text(word)
                        }
                    }
                }
                Section{
                    Text("Current score is:")
                    Text("\(wordScore) \(wordScore > 1 ? "words" : "word")")
                    Text("\(characterScore) \(characterScore > 1 ? "letters" : "letter")")
                }
            }
            .navigationTitle(rootWord)
            .onSubmit(addNewWord)
            .onAppear(perform: startGame)
            .toolbar {
                Button("New game", action: startGame)
            }
            .alert(errorTittle, isPresented: $showingAllert) {
                Button("OK") {}
            } message: {
                Text(errorMessage)
            }
        }
        
    }
    
    
    func addNewWord() {
        let answer = newWord.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        guard answer.count > 0 else { return }
        
        guard isOriginal(word: answer) else {
            wordError(title: "Word is already used", message: "Be more original")
            return
        }
        
        guard isPossible(word: answer) else {
            wordError(title: "Word not possible", message: "You can't spell that word from '\(rootWord)'")
            return
        }
        
        guard isReal(word: answer) else {
            wordError(title: "Word not recognized", message: "You can't just make them up, you know")
            return
        }
        
        guard isTooShort(word: answer) else {
            wordError(title: "Word is too short", message: "You can't use word shorter than 3")
            return
        }
        
        guard isRootWord(word: answer) else {
            wordError(title: "It is root word", message: "You can't use root word")
            return
        }
        
        withAnimation{
            usedWords.insert(answer, at: 0)
        }
        wordScore += 1
        characterScore += answer.count
        newWord = ""
    }
    func startGame() {
        usedWords = []
        characterScore = 0
        wordScore = 0
        if let startWordsURL = Bundle.main.url(forResource: "start", withExtension: "txt") {
            if let startWords = try? String(contentsOf: startWordsURL) {
                let allWords = startWords.components(separatedBy: "\n")
                rootWord = allWords.randomElement() ??  "silkworm"
                return
            }
        }
    }
    func isOriginal(word: String) -> Bool {
        !usedWords.contains(word)
    }
    func isPossible(word: String) -> Bool {
        var tempWord = rootWord
        
        for letter in word {
            if let pos = tempWord.firstIndex(of: letter) {
                tempWord.remove(at: pos)
            } else {
                return false
            }
        }
        return true
    }
    func isReal(word: String) -> Bool {
        let checker = UITextChecker()
        let range = NSRange(location: 0, length: word.utf16.count)
        let misspelledRange = checker.rangeOfMisspelledWord(in: word, range: range, startingAt: 0, wrap: false, language: "en")
        return misspelledRange.location == NSNotFound
    }
    
    func isTooShort(word: String) -> Bool {
        if word.count < 3 {
            return false
        }
        return true
    }
    
    func isRootWord(word: String) -> Bool {
        if word == rootWord {
            return false
        }
        return true
    }
    
    func wordError(title: String, message: String){
        errorTittle = title
        errorMessage = message
        showingAllert = true
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
